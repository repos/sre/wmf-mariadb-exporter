
import yaml
from prometheus_client import start_http_server


def load_config(config_file):
    with open(config_file, 'r') as file:
        return yaml.safe_load(file)


def main():
    config = load_config('config.yaml')

    start_http_server(config['server']['port'])

    if 'sql-queries' in config['probes']:
        import sql_queries
        result = sql_queries.monitor(config["probes"]["sql-queries"])
        print(result)
        import sys
        sys.exit(2)


if __name__ == '__main__':
    main()
