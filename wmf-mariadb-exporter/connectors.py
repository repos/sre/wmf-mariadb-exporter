import pymysql
import os
import re

PATH = "/var/lib/prometheus"
cnf = re.compile(r'^\.my\.([a-zA-Z0-9]+)\.cnf$')


def connect_to_dbs():
    database_found = {}

    # Ensure the directory exists
    if not os.path.exists(PATH):
        raise FileNotFoundError(f"No default file found in {PATH}")

    # List all default files in the directory
    for filename in os.listdir(PATH):
        if filename == ".my.cnf":
            database_found['default'] = pymysql.connect(
                read_default_file=PATH+"/"+filename,)
            return database_found  # We're on a single instance node
        elif cnf.match(filename):  # We're on a multi-instance node
            database_found[cnf.match(filename).group(1)] = pymysql.connect(
                read_default_file=filename,
            )
    return database_found


if __name__ == "__main__":
    print(connect_to_dbs())
