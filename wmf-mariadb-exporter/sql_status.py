#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from dataclasses import dataclass
import re


@dataclass
class Status:
    slave_io_state: str
    master_host: str
    master_user: str
    master_port: int
    connect_retry: int
    master_log_file: str
    read_master_log_pos: int
    relay_log_file: str
    relay_log_pos: int
    relay_master_log_file: str
    slave_io_running: str
    slave_sql_running: str
    replicate_do_db: str
    replicate_ignore_db: str
    replicate_do_table: str
    replicate_ignore_table: str
    replicate_wild_do_table: str
    replicate_wild_ignore_table: str
    last_errno: int
    last_error: str
    skip_counter: int
    exec_master_log_pos: int
    relay_log_space: int
    until_condition: str
    until_log_file: str
    until_log_pos: int
    master_ssl_allowed: str
    master_ssl_ca_file: str
    master_ssl_ca_path: str
    master_ssl_cert: str
    master_ssl_cipher: str
    master_ssl_key: str
    seconds_behind_master: int
    master_ssl_verify_server_cert: str
    last_io_errno: int
    last_io_error: str
    last_sql_errno: int
    last_sql_error: str
    replicate_ignore_server_ids: str
    master_server_id: int
    master_ssl_crl: str
    master_ssl_crlpath: str
    using_gtid: str
    gtid_io_pos: str
    replicate_do_domain_ids: str
    replicate_ignore_domain_ids: str
    parallel_mode: str
    sql_delay: int
    sql_remaining_delay: str
    slave_sql_running_state: str
    slave_ddl_groups: int
    slave_non_transactional_groups: int
    slave_transactional_groups: int


def parse_status(status) -> Status:
    result = {}
    for line in status.splitlines():
        if "***************************" in line:
            rows += 1
            continue
        if rows > 1:
            raise NotImplementedError(
                "Multimaster context, not implemented.")
        if ":" in line:
            try:
                key, value = line.split(":", 2)
                if re.match(r'^(?=\s*$)', value):  # if empty string we assign Nonetype instead
                    value = None
                else:
                    value = value.strip()
                result[key.strip().lower().replace(' ', '_')] = value
            except ValueError:
                key = line.split(":", 1)
                result[key.strip().lower().replace(' ', '_')] = None
            except AttributeError:
                continue
    return Status(**result)
