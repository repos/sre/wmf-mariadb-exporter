#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import time
import multiprocessing

import connectors
import pymysql
from sql_status import parse_status, Status

sql_queries = {}
pthbquery = ('SELECT greatest(0, TIMESTAMPDIFF(MICROSECOND, max(ts), UTC_TIMESTAMP(6)) - 500000)/1000000 '
             'FROM `heartbeat`.`heartbeat` ORDER BY ts LIMIT 1;')
sql_queries['ptheartbeat'] = {"query": pthbquery, "parser": False}
# sql_queries['replicastatus'] = {
#     "query": "SHOW SLAVE STATUS;", "parser": True, "parser_name": "status"}

output_queue = multiprocessing.Queue()
timeout = 10

parsers = {"status": Status, "method": parse_status}


def monitor(config):
    res = []
    processes = []
    instances = connectors.connect_to_dbs()
    for query in sql_queries:
        for instance in instances:
            if query in config or config == "":
                print(sql_queries[query])
                parser = None
                if sql_queries[query]['parser']:
                    print("Parser required")
                    parser_name = sql_queries[query]['parser_name']
                    parser = parsers[parser_name]
                process = multiprocessing.Process(
                    target=run_query, args=(output_queue, sql_queries[query], instances[instance], instance, parser))
                processes.append(process)
                process.start()

        for process in processes:
            process.join(timeout)

        for process in processes:
            if process.is_alive():
                process.terminate()
                process.join()

        while not output_queue.empty():
            query, instance_name, query_result = output_queue.get()
            res.append({"query": query, "instance": instance_name,
                        "result": float(query_result)})
    return res


def run_query(output_queue, query_info, conn, instance_name, parser):
    cursor = conn.cursor()
    query = query_info['query']
    print(query, parser)
    try:
        cursor.execute(query)
        query_result = cursor.fetchall()
        if parser:
            # Placeholder for parser logic
            query_result = parser(query_result)
        else:
            query_result = query_result[0][0] if query_result else None
        output_queue.put((query, instance_name, query_result))
    except (pymysql.err.ProgrammingError, pymysql.err.InternalError) as e:
        print(f"Error: {e}")
        raise RuntimeError(
            f"An error occurred while executing {query}@{instance_name}")
    except IndexError:
        # TODO: scraping error counter inc() and alert about this
        # for now, skipping a query that would return an empty set,
        # for instance `SHOW SLAVE STATUS;` on a replication source
        pass
    finally:
        cursor.close()


if __name__ == "__main__":
    DB = """CREATE DATABASE IF NOT EXISTS heartbeat;"""
    TABLE = """CREATE TABLE IF NOT EXISTS `heartbeat`.`heartbeat` (
    `ts` VARBINARY(26) NOT NULL,
    `server_id` INT(10) UNSIGNED NOT NULL,
    `file` VARBINARY(255) DEFAULT NULL,
    `position` BIGINT(20) UNSIGNED DEFAULT NULL,
    `relay_master_log_file` VARBINARY(255) DEFAULT NULL,
    `exec_master_log_pos` BIGINT(20) UNSIGNED DEFAULT NULL,
    `shard` VARBINARY(10) DEFAULT NULL,
    `datacenter` BINARY(5) DEFAULT NULL,
    PRIMARY KEY (`server_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=binary;
    """
    INSERT = """INSERT INTO `heartbeat`.`heartbeat` (`server_id`, `ts`)
                VALUES (1, NOW())
                ON DUPLICATE KEY UPDATE `ts` = NOW();
            """
    # the following is **NOT** for production
    conn = connectors.connect_to_dbs()
    for db in conn:
        c = conn[db].cursor()
        c.execute(DB)
        c.execute(TABLE)
        c.execute(INSERT)
        conn[db].commit()
        time.sleep(1)
    print(monitor(""))
