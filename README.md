# WMF MariaDB Monitoring

Given WMF's MariaDB topology, we need some tweaks that off the shelf monitoring does not provide.

## How it connects to MariaDB

wmf-mariadb-exporter is designed to run under `prometheus` user account, using `prometheus` default `~/.my.cnf` and `~/.my.{section}.cnf` as default files for `pymysql`.